from socket import AF_INET, socket, SOCK_STREAM
from threading import Thread
from clint.textui import puts, colored, indent
import click
import sys

HEADER_LENGTH = 10
IP = '127.0.0.1'
BUFSIZ = 1024
clients = {}
addresses = {}


def receive(client_socket):
    while True:
        try:
            msg = client_socket.recv(BUFSIZ).decode("utf8")
            with indent(4, quote='>>>'):
                puts(colored.blue(msg))
        except OSError:  # Possibly client has left the chat.
            break


def send(client_socket):
    while True:
        msg = input()
        client_socket.send(bytes(msg, "utf8"))
        if msg == "{quit}":
            client_socket.close()
            break
    sys.exit()


@click.command()
@click.option('-sip', default="localhost", help='Server IP')
@click.option('-sp', default=8000, help='Port')
def main(sip, sp):
    addr = (sip, sp)
    client_socket = socket(AF_INET, SOCK_STREAM)
    client_socket.connect(addr)
    receive_thread = Thread(target=receive, args=(client_socket, ))
    receive_thread.start()
    send_thread = Thread(target=send, args=(client_socket, ))
    send_thread.start()


if __name__ == "__main__":
    main()
