from socket import AF_INET, socket, SOCK_STREAM
from threading import Thread
import click
import sys

HEADER_LENGTH = 10
IP = '127.0.0.1'
BUFSIZ = 1024
clients = {}
addresses = {}


def accept_incoming_connections(server):
    while True:
        client, client_address = server.accept()
        print("client_address")
        print(F"{client_address} has connected.")
        client.send(
            bytes("Connected to server. Start sending messages!\nEnter your Name:\n", "utf8"))
        addresses[client] = client_address
        Thread(target=handle_client, args=(client, client_address)).start()


def handle_client(client, client_address):
    name = client.recv(BUFSIZ).decode("utf8")
    welcome = F"Welcome {name}! If you ever want to quit, type {{quit}} to exit.\n"
    client.send(bytes(welcome, "utf8"))
    msg = F"{name} has joined the chat!\n"
    broadcast(bytes(msg, "utf8"), client_address)
    clients[client] = client_address

    while True:
        msg = client.recv(BUFSIZ)
        if msg != bytes("{quit}", "utf8"):
            broadcast(msg, client_address, F"<{client_address}> : ")
        else:
            broadcast(
                bytes(F"<{client_address}> has left the chat.\n", "utf8"), client_address)
            client.close()
            del clients[client]
            print(F"{client_address} has disconnected.")
            break


def broadcast(msg, addr, prefix=""):
    for sock in clients:
        if addr != clients[sock]:
            sock.send(bytes(prefix, "utf8")+msg)


def command_to_server(server):
    while True:
        msg = input()
        if msg == "{stop}":
            server.close()
            break
    sys.exit()


@click.command()
@click.option('-sp', default=8000, help='Port')
def main(sp):
    addr = (IP, sp)
    server = socket(AF_INET, SOCK_STREAM)
    server.bind(addr)
    server.listen(5)
    print(F"Server Initialized on port {sp}")
    print(F"Send {{stop}} incase u want to stop server.")
    Thread(target=command_to_server, args=(server,)).start()
    accept_thread = Thread(target=accept_incoming_connections, args=(server,))
    accept_thread.start()
    accept_thread.join()
    server.close()


if __name__ == "__main__":
    main()
